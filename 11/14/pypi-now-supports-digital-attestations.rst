.. index::
   pair: Attestations numériques; Digital attestations
   ! Digital attestations

.. _pypi_attestations_2024_11_14:

================================================================================
2024-11-14 **PyPI now supports digital attestations** by Dustin Ingram
================================================================================

- https://docs.pypi.org/attestations/
- https://blog.pypi.org/posts/2024-11-14-pypi-now-supports-digital-attestations/
- https://peps.python.org/pep-0740/ (Index support for digital attestations)
- https://blog.trailofbits.com/2024/11/14/attestations-a-new-generation-of-signatures-on-pypi/
- https://trailofbits.github.io/are-we-pep740-yet/
- https://docs.pypi.org/attestations/producing-attestations/


.. tags:: packaging


Introduction
================

PyPI package maintainers can now publish signed digital attestations when 
publishing, in order to further increase trust in the supply-chain security 
of their projects. 

Additionally, a new API is available for consumers and installers to 
verify published attestations.

Many projects have already begun publishing attestations, with more than 
20,000 attestations already published.

This finalizes PyPI's support for PEP 740, and follows directly from 
previous work to add support for Trusted Publishing, as well as the 
deprecation and removal of PGP signatures.

Why not plain signatures ?
================================

PyPI's support for digital attestations has three key advantages over 
regular cryptographic signatures, such as those provided by PGP:

- **Attestations are signed by an identity, not a key pair**: 
  Similar to our recent support for Trusted Publishing, PyPI's support 
  for digital attestations relies upon **Open ID Connect (OIDC) identities**. 
  By signing attestations with identities, and not a public/private key pair, 
  we mitigate the potential for an individual's key loss or compromise, 
  one of the most common failure cases for PGP signing.
- **Attestations provide a verifiable link to an upstream source repository**: 
  By signing with the identity of the upstream source repository, such as 
  in the case of an upload of a project built with GitHub Actions, 
  PyPI's support for digital attestations defines a strong and verifiable 
  association between a file on PyPI and the source repository, workflow, 
  and even the commit hash that produced and uploaded the file. 
  Additionally, publishing attestations to a transparency log helps mitigate 
  against both compromise of PyPI and compromise of the projects themselves.
- **Attestations are verified when uploaded, and must be verifiable to be 
  uploaded**: Upon review of the state of historical PGP signatures published 
  to PyPI, we found that many signatures were not verifiable, either by 
  PyPI or by end users. 
  With support for PEP 740, PyPI only permits attestations with a verifiable 
  signature to be uploaded and redistributed by the index. 
  This ensures that all attestations are verifiable and useful for all PyPI users.


Much more detail is provided in a corresponding blog post by Trail of Bits: `Attestations: a new generation of signatures on PyPI https://blog.trailofbits.com/2024/11/14/attestations-a-new-generation-of-signatures-on-pypi/ <https://blog.trailofbits.com/2024/11/14/attestations-a-new-generation-of-signatures-on-pypi/>`_.


How to view a file's attestations
======================================

For consumers and package installers wanting to perform verification, 
PyPI currently provides two ways to access digital attestations associated 
with a given file on PyPI:

- A new Integrity API for PyPI The Integrity API provides programmatic 
  access to PyPI's implementation of PEP 740. 
  Operating on individual files, it collects all published attestations 
  for a given file and returns them as a single response.
- A new web UI for viewing per-file attributes 
  Similarly, we have introduced  a new page on PyPI's web UI, displaying 
  details for individual files, including the presence of any attestations 
  about the file. 
  You can see an example here: https://pypi.org/project/sampleproject/#sampleproject-4.0.0.tar.gz

Get started today
===========================

- https://github.com/pypi/warehouse/issues/17001 (PEP 740: Post-deployment tasks)
- https://docs.pypi.org/attestations/producing-attestations/#the-manual-way
- https://docs.pypi.org/attestations/producing-attestations/


The generation and publication of attestations happens by default, and 
no changes are necessary for projects that meet all of these conditions:

- publish from GitHub Actions;
- via Trusted Publishing; and
- use the pypa/gh-action-pypi-publish action to publish.

Support for automatic attestation generation and publication from other 
Trusted Publisher environments `is planned (PEP 740: Post-deployment tasks) https://github.com/pypi/warehouse/issues/17001 <https://github.com/pypi/warehouse/issues/17001>`_. 

While not recommended, maintainers can also `manually generate and publish attestations https://docs.pypi.org/attestations/producing-attestations/#the-manual-way <https://docs.pypi.org/attestations/producing-attestations/#the-manual-way>`_.


Acknowledgements
==============================

- https://www.sovereign.tech/tech/python-package-index
- https://www.sigstore.dev/
- https://pypi.org/project/sigstore/
- https://github.com/webknjaz

Support for work on PEP 740's authoring and design was provided by the 
Sovereign Tech Agency and the Google Open Source Security Team.

Funding for the implementation of PEP 740 was provided by the 
Google Open Source Security Team, and much of the development work on 
PyPI and related tools was performed by Trail of Bits, with special 
thanks to contributors William Woodruff, Facundo Tuesca, and Alexis Challande.

Thanks to the the Sigstore project for their work popularizing identity-based 
signing, hosting a public-good transparency log, and continued support 
of the Python client for Sigstore.

Many thanks to Sviatoslav Sydorenko as well for his support and ongoing 
maintenence of the pypa/gh-action-pypi-publish action, as well his support 
for implementing PEP 740 in the action.

What are attestations ?
===============================

- https://trailofbits.github.io/are-we-pep740-yet/ 

Attestations are digitally signed, publicly verifiable statements about 
Python packages, including their provenance (e.g., the exact source 
repository that produced them).

Attestations are built on top of Sigstore and use short-lived signing 
keys bound to trusted identities (like Trusted Publishers), making them 
misuse-resistant and less susceptible to key loss and theft. 

2024-11-14 Python forum
================================

- https://discuss.python.org/t/pypi-now-supports-digital-attestations/71158
- https://docs.pypi.org/attestations/

PSA: PyPI’s support for PEP 740 is now live, and enabled by default for 
packages that use Trusted Publishing with the canonical PyPA pypi-publish action!

The switch was silently flipped a few weeks ago, but today makes it 
official. 

Since enablement, we’ve seen over 20,000 attestations uploaded to PyPI 
and 20/360 (so far) of the top packages upload attestations. 

The latter can be tracked with `Are we PEP 740 yet? https://trailofbits.github.io/are-we-pep740-yet/ <https://trailofbits.github.io/are-we-pep740-yet/>`_

User docs: `Introduction - PyPI Docs https://docs.pypi.org/attestations/ <https://docs.pypi.org/attestations/>`_

More details on the PyPI blog: `PyPI now supports digital attestations - The Python Package Index Blog https://blog.pypi.org/posts/2024-11-14-pypi-now-supports-digital-attestations/ <https://blog.pypi.org/posts/2024-11-14-pypi-now-supports-digital-attestations/>`_

My colleagues and I have also written an internals-focused blog post: `Attestations: A new generation of signatures on PyPI | Trail of Bits Blog https://blog.trailofbits.com/2024/11/14/attestations-a-new-generation-of-signatures-on-pypi/ <https://blog.trailofbits.com/2024/11/14/attestations-a-new-generation-of-signatures-on-pypi/>`_

