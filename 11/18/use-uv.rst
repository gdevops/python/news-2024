


================================================================================
2024-11-18 Introduce uv for Python dependencies management by David Guillot
================================================================================

- https://github.com/wsvincent/lithium
- https://github.com/wsvincent/lithium/pull/148
- https://github.com/wsvincent/lithium/compare/main...David-Guillot:lithium:feat/use-uv
- https://github.com/David-Guillot/lithium/tree/feat/use-uv
- https://github.com/David-Guillot/lithium/blob/feat/use-uv/Dockerfile
- https://github.com/David-Guillot/lithium/blob/feat/use-uv/README.md

::

    RUN apt-get update && apt-get install -y libpq-dev
