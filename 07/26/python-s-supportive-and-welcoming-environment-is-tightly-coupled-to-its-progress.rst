.. index::
   ! Community Guidelines

.. _nicholson_2024_07_26:
.. _community_2024_07_26:

=================================================================================================================================================
2024-07-26 **Python’s Supportive and Welcoming Environment is Tightly Coupled to Its Progress, Community Guidelines** by Deb Nicholson
=================================================================================================================================================


Developer : Deb Nicholson
===============================

- https://discuss.python.org/u/Deb/summary
- https://discuss.python.org/t/the-psfs-new-executive-director-deb-nicholson/14893
- https://pyfound.blogspot.com/2022/04/deb-nicholson-new-executive-director.html


Presentation 
===============

- https://discuss.python.org/guidelines/

Hi there !

We’re introducing `Community Guidelines <Community Guidelines>`_ that are tailored to this space in 
addition to the Python Software Foundation Code of Conduct that we’re bound by.

We hope this will clarify questions that arose over the past few weeks.

Please have a read and reach out to @moderators with any questions.


Python’s Supportive and Welcoming Environment is Tightly Coupled to Its Progress
======================================================================================

- https://discuss.python.org/t/python-s-supportive-and-welcoming-environment-is-tightly-coupled-to-its-progress/59232


Python is as popular as it is today because we have gone above and beyond to
make this a welcoming community. Being a friendly and supportive community is
part of how we are perceived by the wider world and is integral to the wide
popularity of Python. 

We won a “Wonderfully Welcoming Award” last year at GitHub Universe. 

Over and over again, the tech press refers to Python as a supportive community. 

We aren’t the fastest, the newest or the best-funded programming language, 
but we are the most welcoming and supportive. 

Our philosophy is a big part of why Python is a fantastic choice for not only new
programmers, glue programmers, and folks who split their time between research
and programming but for everyone who wants to be part of a welcoming community.

We believe to be “welcoming” means to do our best to provide all
participants with a safe, civil, and respectful environment when they are
engaging with our community - on our forums, at PyCon events, and other
spaces that have committed to following our Code of Conduct . 

That kind of environment doesn’t happen by accident - a lot of people have worked hard
over a long time to figure out the best ways to nurture this welcoming quality
for the Python community. 

That work has included drafting and improving the Code of Conduct, crafting 
and implementing processes for enforcing it, and moderating the various online 
spaces where it applies. 

**And most importantly the huge, collective effort of individuals across 
the community, each putting in consistent effort to show up in all the 
positive ways that make the Python community the warm and welcoming place 
that we know**.

The recent slew of conversations, initially kicked off in response to a
bylaws change proposal, has been pretty alienating for many members of our
community. 

They haven’t all posted publicly to explain their feelings,
but they have found other ways to let the PSF know how they are feeling.

...

We have a moral imperative 
==================================


We have a moral imperative – as one of the very best places to bring new
people into tech and into open source – to keep being good at welcoming
new people. If we do not rise and continue to rise every day to this task,
then we are not fulfilling our own mission, “to support and facilitate the
growth of a `diverse https://www.python.org/psf/diversity/ <https://www.python.org/psf/diversity/>`_ 
and international community of Python programmers.”

Technical skills are a game-changer for the people who acquire them and
joining a vast global network of people with similar interests opens many
doors. Behavior that contributes to a hostile environment around Python or
throws up barriers and obstacles to those who would join the Python community
must be addressed because it endangers what we have built here.


We can not tolerate intolerance
=====================================

- https://en.wikipedia.org/wiki/Paradox_of_tolerance

Part of the caretaking of a diverse community “where everyone feels
welcome” sadly often means asking some people to leave – or at least
take a break. 

This is known as the `paradox of tolerance <https://en.wikipedia.org/wiki/Paradox_of_tolerance>`_. 

We can not tolerate intolerance and we will not allow combative and aggressive 
behavior to ruin the experience in our spaces for everyone else. 

People do make honest mistakes and don’t always understand the impact that 
their words have had. 

All we ask is that as community members we all do our best to adhere to 
the Code of Conduct we’ve committed to as a community, and that we gracefully 
accept feedback when our efforts fall short. 

Sometimes that means learning that the words, assumptions or tone you’re 
using aren’t coming across the way you’ve intended. 

When a person’s words and actions repeatedly come in conflict with our 
community norms and cause harm, and that pattern hasn’t changed
in response to feedback – then we have to ask people to take a break or
as a last resort to leave the conversation.

We especially want to thank all the volunteers who serve on the Python Discourse moderation team and our Code of Conduct Working Group
============================================================================================================================================

- https://discuss.python.org/
- https://wiki.python.org/psf/ConductWG/Charter

Our forum, mailing lists and events will continue to be moderated. We want
to thank everyone who contributed positively to the recent conversations and
everyone who made the hard choice to write to us to point out off-putting,
harmful, unwelcoming or offensive comments. 

We especially want to thank all the volunteers who serve on the `Python Discourse <https://discuss.python.org/>`_ 
moderation team and our `Code of Conduct Working Group <https://wiki.python.org/psf/ConductWG/Charter>`_. 

We know it’s been a long couple of weeks, and although your work may occasionally 
be draining and unpleasant, it is also absolutely essential and endlessly 
appreciated by the vast majority of the community. 

Thank you for everything you do!

Sincerely,

- Deb Nicholson
- Dawn Wages
- Tania Allard
- KwonHan Bae
- Kushal Das
- Georgi Ker
- Jannis Leidel
- Cristián Maureira-Fredes
- Christopher Neugebauer
- Denny Perez
- Cheuk Ting Ho
- Simon Willison
