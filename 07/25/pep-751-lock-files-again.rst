

.. _cannon_2024_07_25:

==================================================================
2024-07-25 **PEP 751: lock files (again)** by Brett Cannon
==================================================================

PEP 751: lock files (again)
=================================

- https://discuss.python.org/t/pep-751-lock-files-again/59173
- https://discuss.python.org/u/brettcannon/summary
- https://discuss.python.org/t/lock-files-again-but-this-time-w-sdists/46593/

This was all last discussed in `Lock files, again (but this time w/
sdists!) <https://discuss.python.org/t/lock-files-again-but-this-time-w-sdists/46593/>`_. 

Probably the biggest change since the initial post of that topic is **adding 
support for per-package locking instead of only per-file locking** 
(it’s explained  in the PEP what those terms mean). 

I also focused on making the format work well when read as a diff for changes, 
so there’s a bit more information for people to understand why a package 
got pulled in along w/ minimizing having to read other parts of the file 
(when you look at the per-file locking example at `mousebender/pylock.example.toml 
at pep brettcannon/mousebender <https://github.com/brettcannon/mousebender/blob/pep/pylock.example.toml>`_· 

GitHub you can also look at a couple of recent commit diffs I did just to show what a version change looks like). This also
means the file itself is smaller than my initial proposal.

And while I thank them in the acknowledgements section of the PEP, I want to
thank again @pf_moore , @radoering of Poetry, @ofek of Hatch, and @sethmlarson
of Python security for reading the initial draft of this PEP back in March
(the PEP hasn’t changed substantially since then; mostly tightening up
some ambiguity).



PEP 751 – A file format to list Python dependencies for installation reproducibility
----------------------------------------------------------------------------------------

- https://peps.python.org/pep-0751/

Motivation
-------------------

Currently, no standard exists to:

- Specify what top-level dependencies should be installed into a Python environment.
- Create an immutable record, such as a lock file, of which dependencies were installed.

Considering there are at least five well-known solutions to this problem 
in the community (pip freeze, pip-tools, uv, Poetry, and PDM), **there seems 
to be an appetite for lock files in general**.


