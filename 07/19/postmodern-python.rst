
.. index::
   pair: Chris Arderne; Beyond Hypermodern: Python is easy now

.. _arderne_2024_07_19:

==================================================================================
2024-07-19 **Beyond Hypermodern: Python is easy now** by Chris Arderne
==================================================================================

- https://rdrn.me/postmodern-python/
- https://github.com/carderne
- https://github.com/carderne.atom
- https://github.com/carderne/postmodern-python

Developer: Chris Arderne
============================

- https://rdrn.me/

I’m a software/infra/data engineer and love building and breaking and 
tinkering. 

If I’m inside but not at the computer, I’m probably reading. 
And when outside, I’d ideally be climbing mountains but more frequently 
running and cycling.


Beyond Hypermodern: Python is easy now
==========================================

It feels like eons, but it was actually just four years ago that 
Hypermodern Python did the rounds, going over the latest Best Practises™ 
for Python tooling. 

I remember reading it with a feeling of panic: I need to install like 
20 packages, configure 30 more and do all this stuff just to write some Python.

But now it’s 2024, and it’s finally all easy! 

A bunch of people saw how easy all this stuff was in Go and Rust, and 
did some amazing work to drag the Python ecosystem forward. 

It’s no longer clever or special to do this stuff; everyone should be doing it.

If you just want the template, it’s coming below in the TLDR. 

Otherwise hang in, I’m going to follow much the same structure as the 
original Hypermodern posts, as follows:

- Setup
- Linting
- Typing
- Testing
- Documentation
- CI/CD
- `Monorepo (bonus section!) <https://rdrn.me/postmodern-python/#monorepo>`_

If you’re already using Rye and friends, much of this won’t be new to you. 

But the monorepo section is where things get more interesting and there 
might be some new ideas there!

TLDR Here’s the template repository: `carderne/postmodern-python <https://github.com/carderne/postmodern-python>`_

Start from there, and the README will give you all the commands you need to know.
