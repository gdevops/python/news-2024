

==================================================================
2024-07-18
==================================================================


.. toctree::
   :maxdepth: 3

   the-python-docs-editorial-board-now-has-a-changelog-to-document-decisions
   python-3-13-0b4-now-available
