

.. _python_2024_07_18:

=============================================================================================================
2024-07-18 **The Python Docs Editorial Board now has a "Changelog" to document decisions (and the timeline)**
=============================================================================================================


- https://fosstodon.org/@mariatta/112808395045408262
- https://discuss.python.org/t/editorial-board-decisions/58580
- https://github.com/python/editorial-board/blob/main/CHANGELOG.md


The first of such decisions is for adding to the Python docs style guide 
about use of / and * in function arguments.

See the announcement (by Docs Editorial Board member and newest Python 
core team member, @nedbat): https://discuss.python.org/t/editorial-board-decisions/58580

Read the changelog (just one item for now): https://github.com/python/editorial-board/blob/main/CHANGELOG.md
