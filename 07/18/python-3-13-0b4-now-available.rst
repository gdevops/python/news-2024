

.. _python_3_13_beta4_2024_07_18:

==================================================================
2024-07-18 Python 3.13.0b4 now available
==================================================================

- https://discuss.python.org/t/python-3-13-0b4-now-available/58565
- https://www.python.org/downloads/release/python-3130b4/

Python 3.13’s final beta, the last chance to fix bugs ever (before the 
release candidate phase), is finally released. 

The 3.13 branch is unlocked for now, but please take great care with 
backporting fixes, as we’re now getting very close to the release and we 
do not want last-minute fixes accidentally breaking tests and introducing 
compiler warnings, Łukasz.
