

.. _stuart_2024_07_07:

==============================================================================
2024-07-07 **Modern Good Practices for Python Development** by Stuart Ellis 
==============================================================================

- https://www.stuartellis.name/articles/python-modern-practices


Author: Stuart Ellis 
======================

- https://github.com/stuartellis
- https://www.stuartellis.name/

Install Python With Tools That Support Multiple Versions 
==========================================================

Use a tool like mise or pyenv to install Python on your development systems, 
so that you can switch between different versions of Python for your projects. 

This enables you to upgrade each project to a new version of Python without 
interfering with other tools and projects that use Python.


Developing Python Projects 
============================

Avoid Using Poetry 
---------------------

Avoid using Poetry for new projects. 

Poetry predates many standards for Python tooling. 

This means that it uses non-standard implementations of key features, 
such as the dependency resolver and configuration formats in pyproject.toml files.

If you would like to use a similar tool to develop your applications, 
consider using PDM. 

Hatch is another alternative to Poetry, but it is most useful for developing 
Python libraries. 

**Both of these tools follow modern standards, which avoids compatibility issues**.

