.. index::
  pair: Larry Du ; Python has too many package managers (2024-07-06)

.. _larry_du_2024_07_06:

===============================================================
2024-07-06 **Python has too many package managers** by Larry Du
===============================================================

- https://dublog.net/blog/so-many-python-package-managers/


Author : Larry Du
=====================

- https://dublog.net/about/index.html


The sane way to do things
===============================

In a sane world, package management would work like it does with Cargo 
the rust package manager. You have a single master configuration TOML 
file where you simply list your dependencies and config settings. 

The TOML file goes into a folder encapsulating your entire development 
environment. 

For extra reproducibility, whenever you build your environment and resolve 
all your package dependencies, a \*.lock file records all the packages 
you used along with their versions and hashes.

Finally, because dependency resolution is a directed acylic graph (DAG) 
resolution problem, the dependency retrieval and resolution should both b
e engineered to be relatively fast. 

Dependency information should be freely available from a public API metadata 
server in a way that is simple to parse, and cached locally once downloaded 
to avoid redundantly hitting this server. 

Finally, the dependency resolution algorithm itself should be written in 
a relative fast programming language like C++ or Rust.

The problem with Python has been that there hasn’t been a single tool 
that does all of this well although some have come enticingly close. 

To that end, here is my rundown of more than a dozen Python package 
managment/virtual environment tools:


pyenv
======

One thing to note about python’s venv tool is that it isn’t really setup 
to create virtual environments for different versions of Python. 

To actually do this, yet another tool called pyenv exists which allows 
you to switch between different versions of system Python at will, with 
options to set Python locally for specific projects. 

Very often, I’ve seen this tool abused to set Python versions globally, 
which can lead to some severe reproducibility issues, with folks forgetting 
which version of Python they were using for different projects.

PEP 621 Storing project metadata in pyproject.toml
=====================================================

- :ref:`tuto_python:pep_621`

In 2020 PEP 621 was accepted. 

This proposal effectively gives a guidance to consolidate everything into 
a pyproject.toml file, almost identical to Cargo.toml in Rust and similar 
to the package.json used in npm. 

Naturally this led to a proliferation of new Python package managers which 
leverage the new standard. 

Enter poetry, PDM, Flit, and Hatch.


.. _poetry_2024_07_06:

2024-07-06 Poetry
=========================

Poetry right now is the closest tool in the Python ecosystem with widespread
traction that actually comes close to approximating the experience of using
tools like Cargo and npm. 

Unlike pip and similar to conda and mamba (See below),
Poetry will attempt to resolve the full dependency graph DAG beforehand, and
install dependencies in topological order. It mostly respects pyproject.toml and
treats it as a first-class citizen. Like conda and venv, poetry can also manage
your virtual environments, which can exist within or outside of your project
folder. poetry also generates poetry.lock files which can be an immense boon for
reproducibility. Notably, these lockfiles are multi-platform lock files meaning
they can be extremely large. Finally, poetry is also a build tool, allowing users
to build and publish Python packages rather seamlessly.


Poetry is almost the perfect tool for the job, however it has a number of downsides
--------------------------------------------------------------------------------------

Poetry is almost the perfect tool for the job, however it has a number of
downsides that can be utter deal breakers for production or even basic research
and development. First of all dependency resolution can be incredibly slow. 

A big part of this is no fault of poetry in of itself, but rather in the disparate ways
in which Python packages enumerate their dependencies. Unlike other programming
ecosystems, **not all Python packages declare their metadata in a way that is
necessarily served cleanly by public metadata APIs like PyPI**. 

In these cases, exploring every dependency for every possible package in the DAG can involve
a staggering amount operations to directly figure out package dependencies by
downloading and parsing python wheels. 
For some folks doing basic R&D, the cost of simply having a few packages 
excluded from dependency resolution can be a fairer tradeoff than waiting 
minutes to hours to find a “failure to resolve dependencies”.

Furthermore, as of 2024, the dependency resolver in poetry is actually still written in Python as a depth-first search algorithm
-----------------------------------------------------------------------------------------------------------------------------------

Furthermore, as of 2024, the dependency resolver in poetry is actually still
written in Python as a depth-first search algorithm. By comparison, tools like
mamba have resolvers written in C++ as boolean SAT solvers which are orders of
magnitude faster! Dependency resolves for large projects in poetry, combined with
the generation of multiplatform lockfiles can take an obscene amount of time…
especially when there is an actual conflict in the DAG.

I have actually used poetry at my last job, and one of the number one issues
with the tool is in how most folks (even incredibly experienced folks!) used it
incorrectly to specify dependency bounds on library code intended to be widely
shared. 

In poetry there is the option to use a caret ^ operator implicitly
specify upper and lower bounds. For example, specifying ^0.2.3 is equivalent to
specifying >=0.2.3,<0.3.0. This ceiling pinning seems like a good idea on the
surface, but can wreck havok when applied across a large RnD organization, with
numerous “false positive” unresolvable dependency DAGs when well-intentioned
software engineers apply it too liberally. 

This sort of well intentioned ceiling pinning can have devastating consequences 
for libraries intended to be used widely.

.. _pdm_2024_07_06:

2024-07-06 **pdm** a modern Python package and dependency manager supporting the latest PEP standards
==================================================================================================================

:ref:`pdm <tuto_python:pdm>` is incredibly similar to poetry, but has a 
core difference in that it also supports :ref:`PEP-582 <tuto_python:pep_582>` 

This PEP basically brings pdm in line with other programming language 
environment setups by jettisoning the idea of virtual environments that 
are independent of a given project/folder. 

When you are in your project directory, you are effectively in your virtual 
environment (which is not necessarily totally isolated from the system 
environment and any other active virtual environments). 

This can greatly reduce the futzing around of having to activate and deactivate 
various virtual environment tools in Python. 

pdm is much more compliant with PEP standards than poetry, which can be a killer advantage for certain users
-------------------------------------------------------------------------------------------------------------------

Furthermore, pdm is much more compliant with :ref:`PEP standards than poetry <tuto_python:python_packaging_peps>`, 
which can be a killer advantage for certain users.


.. _pixi_2024_07_06:

2024-07-06 pixi (a drop-in replacement for conda) |pixi|
============================================================

- :ref:`tuto_python:pixi`

One of the most ambitious Rust projects is pixi which seeks to be a drop-in 
replacement for conda. 

Like conda, pixi can manage non-python dependencies. 

In mid 2024, pixi started to switch from its own backend rip to uv (see below) 
for better performance. 

This is an actively evolving tool, and I eagerly await to see where it goes. 

Unlike conda and mamba, pixi incorporates its own type of \*.lock file 
which immediately puts it ahead of vanilla conda for reproducibility.


.. _rye_2024_07_06:

2024-07-06 rye One of the first attempts to redo Python package management in Rust by  Armin Ronacher |rye|
==============================================================================================================

- :ref:`tuto_python:rye`

One of the first attempts to redo Python package management in Rust by 
Armin Ronacher. 

When I first saw this over a year ago, the actual slow part (dependency resolution) 
was simply calling piptools under the hood, leading to no discernable gain 
in speed or performance.

However, over time, **the project has matured to the point where it now 
does most if not all of what poetry does only faster**. 

This project was recently taken over by Astral.sh (developers of uv and 
the ruff linter) and **now uses the same dependency resolver as uv on the 
backend**. 

The tool has also gained a decent amount of traction on some major projects. 

For example, the `OpenAI Python API library https://github.com/openai/openai-python <https://github.com/openai/openai-python>`_ uses it. 

There is a strong possibility that the functionality of rye will eventually 
be fully replicated by uv alone, leading to a merging of the two projects


.. _uv_2024_07_06:

2024-07-06 **uv is by far the most promising package management tool in the Python ecosystem**
==================================================================================================

:ref:`uv <tuto_python:uv>` is by far the most promising package management tool in the Python 
ecosystem as of the writing of this post. 

This project actually aims to be a drop-in replacement for pip on top of 
being a Cargo for Python. 

**The API is currently in no ways stable** (as of 2024), but the benchmarks 
are incredibly promising. 

Most notably, the development is backed by Astral.sh, a company formed by 
Charlie Marsh and the creators of the ruff linter, a widely beloved tool 
that virtually supplanted all incumbants overnight when it was released 
in 2022.

Like poetry, this project supports pyproject.toml, and like pip it uses 
a backtracking approach for dependency resolution. 

**Unlike pip this algorithm is written in Rust and is very fast !** 

Benchmarks show that uv is at least order of magnitude faster than poetry 
when it comes to dependency resolution. 

I fully expect uv to supersede tools like poetry in the future as the 
project matures and API stabilizes, **however as of this writing, it is 
more of a drop-in replacement for various pip tools than an opinionated 
build/packaging/versioning tool like poetry or rye**,

**One promising sign of uv’s performance and adoption is the usage of its 
libraries in other package managers like** :ref:`pixi <tuto_python:pixi>` 
and :ref:`rye <tuto_python:rye>`.

**Verdict: I hope uv takes off and the Python community can one day coalesce around a single standardized tool !**
=====================================================================================================================

Hopefully one day there will be a cohesive solution to bring Python package 
management **to the simplicity and ergonomics seen in the Javascript and 
Rust development ecosystems**. 

Until then, I would simply recommend that most data science/experimentalists 
stick to using conda, and production oriented folks use pip or poetry 
(with some mindfulness towards slow dependency resolve for complex projects 
with poetry).

Fingers crossed, though, I hope uv takes off and the Python community can one day coalesce around a single standardized tool ! 
-----------------------------------------------------------------------------------------------------------------------------------

Fingers crossed, though, I hope :ref:`uv <tuto_python:uv>` takes off and 
the Python community can one day coalesce around a single standardized tool ! 

And there are some promising signs that tools like pixi can improve on 
conda and its wider scoped dependency management.


On the social networks 2024
================================

- - https://x.com/charliermarsh/status/1810519153029369865
