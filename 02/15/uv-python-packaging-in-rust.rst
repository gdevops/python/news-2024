
.. index::
   pair: uv ; Python packaging in Rust
   ! uv

.. _marsch_uv_2024_02_15:

=============================================================
2024-02-15 **uv: Python packaging in Rust** by Charlie Marsh
=============================================================


- https://github.com/astral-sh/uv (An extremely fast Python package installer and resolver, written in Rust)
- https://github.com/mitsuhiko/rye
- :ref:`noam_links:marsch_uv_2024_02_15`
