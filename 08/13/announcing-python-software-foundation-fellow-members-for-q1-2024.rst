

.. _psf_2024_08_13:

=========================================================================================================================
2024-08-13 Announcing Python Software Foundation Fellow Members for Q1 2024 ! Adam Johnson and Paolo Melchiorre  🎉 
=========================================================================================================================

- https://pyfound.blogspot.com/2024/08/announcing-python-software-foundation.html
- https://www.python.org/psf/fellows-roster/


The PSF is pleased to announce its first batch of PSF Fellows for 2024! 
Let us welcome the new PSF Fellows for Q1! The following people continue 
to do amazing things for the Python community:

Adam Johnson
==================

- https://adamj.eu/
- https://fosstodon.org/@adamchainz
- https://github.com/adamchainz/


Paolo Melchiorre 
====================

- https://www.paulox.net/
- https://fosstodon.org/@paulox
- https://github.com/pauloxnet

  - https://github.com/pauloxnet.atom
  
- https://www.youtube.com/@pauloxnet


Thank you for your continued contributions. We have added you to our 
`Fellow roster <https://www.python.org/psf/fellows-roster/>`_.

The above members help support the Python ecosystem by being phenomenal 
leaders, sustaining the growth of the Python scientific community, 
maintaining virtual Python communities, maintaining Python libraries, 
creating educational material, organizing Python events and conferences, 
starting Python communities in local regions, and overall being great 
mentors in our community. 

Each of them continues to help make Python more accessible around the world. 

To learn more about the new Fellow members, check out their links above.

Let's continue recognizing Pythonistas all over the world for their impact 
on our community. 

The criteria for Fellow members is available online: https://www.python.org/psf/fellows/. 
If you would like to nominate someone to be a PSF Fellow, please send a 
description of their Python accomplishments and their email address to 
psf-fellow at python.org. 

Quarter 2 nominations are currently in review. 

We are accepting nominations for Quarter 3 through August 20, 2024.
