.. index::
   ! Brython


.. _brython_2024_04_06:

=================================================================
2024-04-06 **Brython**
=================================================================

- https://brython.info/index.html
- https://www.linux-magazine.com/Issues/2024/278/PyScript/(offset)/3


Conclusion
============

Using Python libraries such as pandas, SymPy, or Matplotlib on a client page
can be a very useful feature.
It's also nice that these PyScript pages don't require Python on the client machine.

While working with PyScript, I found two issues.
**The call-up is very slow (especially compared to Brython pages)**.

In addition, I often got tripped up with Python indentation when I was cutting
and pasting code.

Overall, however, I was very impressed with PyScript, and I look forward to
seeing where the project goes.
