.. index::
   pair:  Fabio Pligier ; pyscript.net Python in the browser


.. _pligier_2024_04_06:

=======================================================================================
2024-04-06 #PyCascades **Next Level Python Applications with PyScript** by Fabio Pliger
=======================================================================================


- https://github.com/pyscript/pyscript
- https://fpliger.pyscriptapps.com/talks-pycascades-2024/latest/
- https://pretalx.com/pycascades-2024/talk/NSEXNX/
- https://2024.pycascades.com/program/schedule/
- https://fpliger.pyscriptapps.com/talks-pycascades-2024/latest/
- https://pyscript.com/@fpliger/talks-pycascades-2024/latest
- https://discord.gg/HxvBtukrg2
- https://community.anaconda.cloud/c/tech-topics/pyscript/41

Slides from Fabio Pliger's #PyCascades talk: Next Level Python Applications with PyScript

Fabio Pliger
===============

Fabio Pliger is the Creator of PyScript and a Principal Software Architect
at Anaconda, Inc, where he has been working for almost 10 years.

Fabio is a fellow member of the PSF and the EuroPython Society, one of the
Founders of the Python Italia association and former Chairman of the EuroPython
Society, where he served from 2012 to 2016.

As part of his work at both associations, he helped organize and co-chair
several Pycons and Europythons over the years.

pyscript examples
===================

- https://pyscript.com/@examples
