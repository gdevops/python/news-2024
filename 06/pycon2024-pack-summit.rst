.. index::
   pair: Python; Packaging

.. _python_packaging_2024_06_01:

====================================================================
2024-06-01 **Packaging Summit at PyCon US 2024** by Pradyun Gedam
====================================================================

- https://discuss.python.org/t/python-packaging-summit-at-pycon-us-2024-notes-and-thanks/54714
- https://hackmd.io/@pradyunsg/pycon2024-pack-summit


I’m excited to share that this is the 1000th topic in the Packaging category !

Oh, and it also has the notes from the `Python Packaging Summit, https://us.pycon.org/2024/events/packaging-summit/ <https://us.pycon.org/2024/events/packaging-summit/>`_ at PyCon US 2024 .
A link to these notes has been added to the event’s page on the PyCon US 2024 website.


I want to thank everyone who attended the event in person, for the discussions
at the summit. I also want to thank everyone who helped with note taking
(with a shoutout to @CAM-Gerlach, since he did most of the heavy lifting d
uring the first few topics !).

This summit would not have happened without the vital support of the PyCon US staff,
thank you all!

And, finally, thank you to my co-organisers @CAM-Gerlach, @FFY00 and @jezdez
for being their awesome selves !
