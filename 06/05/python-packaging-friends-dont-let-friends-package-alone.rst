.. index::
   pair: Leah Wasser ; Friends don’t let friends package alone- my Python packaging talk at PyCon 2024 (2024-06-05) 

.. _wasser_2024_06_05:

================================================================================================================
2024-06-05 **Friends don’t let friends package alone- my Python packaging talk at PyCon 2024** by Leah Wasser
================================================================================================================

- https://www.pyopensci.org/blog/python-packaging-friends-dont-let-friends-package-alone.html
- https://github.com/pyOpenSci/python-package-guide

Developer : Leah Wasser 
==============================

- https://github.com/lwasser
- https://www.leahwasser.com/

I'm Leah (she/her). Pronounced like lay-ah you know like star wars ? 
I'm a data geek, open education and open science advocate.

My work empowers people with the skills needed to get jobs in tech. 
I use my privilege to empower others when I can.

In my free time I'm running the trails in the mountains of Colorado. 
Ultra distances are my thing. 

Learn more about me `here https://www.leahwasser.com/ <https://www.leahwasser.com/>`_.


The evolution of the pyOpenSci Python packaging guide
==========================================================

- https://www.pyopensci.org/python-package-guide/

Years later, what started out as a generic Python development guide, was 
transformed into what’s now the `pyOpenSci Python package guide <https://www.pyopensci.org/python-package-guide/>`_.

- A guidebook that has 58 contributors as I’m writing this post today.
- A guidebook that has color and graphics and that leads Pythonistas 
  down a singular, opinionated path to Python packaging success.
- A guidebook that breaks down pieces and processes into their fundamentals 
  and explains them using digestible words.

The pyOpenSci path is not the only path. 

It is just a path that works and enables early wins for scientists and 
others using Python.

If you’re reading this and you have your own path, that’s OK too. 

But if you want to worry less about decisions and have a definitive way 
of doing things that follows modern standards -

we’ve got you
