.. index::
   pair: Numpy ; 2.0.0 (2024-06-16)

.. _numpy_2_0_0:

============================================
2024-06-16 **Numpy 2.0.0** (2024-06-16)
============================================

- https://github.com/numpy/numpy/releases/tag/v2.0.0
- https://numpy.org/news/
- https://numpy.org/devdocs/release/2.0.0-notes.html

NumPy 2.0.0 is the first major release since 2006
======================================================

It is the result of 11 months of development since the last feature release
and is the work of 212 contributors spread over 1078 pull requests.

It contains a large number of exciting new features as well as changes to
both the Python and C APIs.

This **major release** includes breaking changes that could not happen in a
regular minor (feature) release - including an ABI break, changes to
type promotion rules, and API changes which may not have been emitting
deprecation warnings in 1.26.x.

Key documents related to how to adapt to changes in NumPy 2.0, in addition
to these release notes, include:

- `The NumPy 2.0 migration guide <https://numpy.org/devdocs/numpy_2_0_migration_guide.html>`_
- `The 2.0.0 release notes <https://numpy.org/devdocs/release/2.0.0-notes.html>`_

Documentation
=====================

The reference guide navigation was significantly improved, and
there is now documentation `on NumPy's module structure <https://numpy.org/devdocs/reference/module_structure.html#module-structure>`_,

The building from source documentation was completely rewritten,



Messages on mastodon
=======================

ramikrispin NumPy 2.0.0 was released today! 🥳
----------------------------------------------------

- https://mstdn.social/@ramikrispin/112628165973558402

NumPy 2.0.0 was released today! 🥳

It is also a friendly reminder to update your requirements.txt file with the
NumPy version if your code is running old versions to avoid fun debugging 😂

Checkout the release notes: https://numpy.org/news/

WetHat Writing fast string ufuncs for NumPy 2.0 | Labs
--------------------------------------------------------------

- https://fosstodon.org/@WetHat/112613746347167146

About performance improvements made to string operations in NumPy 2.0 by
reworking how they are handled, moving from creating Python objects to
operating directly on the raw C data buffers.

- https://labs.quansight.org/blog/numpy-string-ufuncs

#Python #NumPy #Programming
