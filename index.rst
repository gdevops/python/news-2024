

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/python/news-2024/rss.xml>`_

.. _python_2024:

==========================
**Python** 2024
==========================


.. toctree::
   :maxdepth: 3

   12/12
   11/11
   08/08
   07/07
   06/06
   04/04
   02/02
   01/01
