.. index::
   pair: Gregory Szorc ; Transferring Python Build Standalone Stewardship to Astral (2024-12-03)

.. _gregory_szorc_2024_12_03:

================================================================================================
2024-12-03 **Transferring Python Build Standalone Stewardship to Astral** by Gregory Szorc
================================================================================================

- https://gregoryszorc.com/
- https://gregoryszorc.com/resume.pdf
- https://gregoryszorc.com/blog/2024/12/03/transferring-python-build-standalone-stewardship-to-astral/
- https://gregoryszorc.com/docs/python-build-standalone/main/
- https://astral.sh/blog/python-build-standalone

Transferring Python Build Standalone Stewardship to Astral
================================================================

My `Python Standalone Builds (PBS) <https://gregoryszorc.com/docs/python-build-standalone/main/>`_ 
project provides self-contained Python distributions that aim to just work.

PBS Python distributions are used by uv, Rye, Bazel's rules_python, and
other tools in the Python ecosystem. 
The GitHub release artifacts have been downloaded over 70,000,000 times.

With ruff and uv, Charlie Marsh and the folks at Astral have been speedrunning
building world class tooling for the Python ecosystem.

As I wrote in My Shifting Open Source Priorities in March 2024, changes in
my life have resulted in me paring back my open source activities.

Since that post, uv has exploded onto the scene. There have been over 50
million downloads of PBS release assets since March.

Charlie and Astral have stepped up and been outstanding open source
citizens when it comes to evolving PBS to support their work. When I told
Charlie I could use assistance supporting PBS, Astral employees started
contributing to the project. They have built out various functionality,
including Python 3.13 support (including free-threaded builds), turnkey
automated release publishing, and debug symbol stripped builds to further
reduce the download/install size. Multiple Astral employees now have GitHub
permissions to approve/merge PRs and publish releases. All releases since
April have been performed by Astral employees.

PBS today is effectively an Astral maintained project and has been that way
for months. As far as I can tell there have been only positive side effects
of this transition. When I ask myself why I should continue being the GitHub
maintainer, every answer distills down to personal pride or building a
personal brand. I need neither.

I agree with Charlie that formally transferring stewardship of my standalone
Python builds project to Astral is in the best interest of not only the
project itself but of the wider Python community.

On 2024-12-17 I will transfer indygreg/python-build-standalone into the
astral-sh GitHub organization. From there, Astral will lead its development
and evolution.

I will retain my GitHub permissions on the project and hope to stay involved
in its development, if nothing more than a periodic advisor.

Astral has clearly demonstrated competency for execution and has the needs
of Python developers at heart. I have no doubt that PBS will thrive under
Astral's stewardship. I can't wait to see what they do next.

Astral has `also blogged https://astral.sh/blog/python-build-standalone <https://astral.sh/blog/python-build-standalone>`_ 
about this announcement and I encourage interested parties to read it as well.
