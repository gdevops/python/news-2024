

.. _uv_2024_12_02:

=======================================================================================
2024-12-02 **Better, Faster Python Projects: A Deep Dive into uv** by SaaS Pegasus
=======================================================================================

- https://www.saaspegasus.com/guides/uv-deep-dive/


Conclusion and resources
=================================

- https://www.youtube.com/watch?v=gSKTfG1GXYQ
- https://simonwillison.net/tags/uv/
- https://www.youtube.com/watch?v=8UuW8o4bHbw 
- https://hynek.me/articles/docker-uv/
- https://blog.pecar.me/uv-with-django

Well, this got a lot longer than I intended, but hopefully it was useful!

As you probably gathered, I'm very bullish on uv and am already adopting 
it in all my projects. I hope this inspires you to try it out and do the same!

I wanted to give a shout out to the following resources, which—in addition 
to the excellent documentation—helped me better understand how to make 
the most of uv:

- I already mentioned this above, but `Charlie Marsh's talk on uv  <https://www.youtube.com/watch?v=gSKTfG1GXYQ>`_
  is a useful overview of both what uv can do and how it works.
- Simon Willison's blog has a `number of practical posts about using uv <https://simonwillison.net/tags/uv/>`_
- Hynek Schlawack has a `great video overview of uv <https://www.youtube.com/watch?v=8UuW8o4bHbw>`_, 
  as well as a good write up on `using uv with Docker <https://hynek.me/articles/docker-uv/>`_
- `Anže Pečar's blog <https://blog.pecar.me/uv-with-django>`_ on uv and Django.

Thanks for reading!
