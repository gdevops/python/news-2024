


.. _zasper_2024_12_22:

=====================================================================================================================
2024-12-22 Zasper: A Supercharged IDE for Data Science, Fastest and Most Efficient IDE to run Jupyter Notebooks
=====================================================================================================================

- https://zasper.io/blog/zasper-intro.html
- https://github.com/zasper-io/zasper


Zasper is an IDE designed from the ground up to support massive concurrency. 

It provides a minimal memory footprint, exceptional speed, and the ability 
to handle numerous concurrent connections.

It’s perfectly suited for running REPL-style data applications, with Jupyter 
notebooks being one example.

Zasper uses one fourth of RAM and one fourth of CPU used by Jupterlab. 

While Jupyterlab uses around 104.8 MB of RAM and 0.8 CPUs, Zasper uses 26.7 MB 
of RAM and 0.2 CPUs.
