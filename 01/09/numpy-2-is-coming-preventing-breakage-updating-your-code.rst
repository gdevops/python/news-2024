

.. _numpy2_2024_01_09:

============================================================================================
2024-01-09 **NumPy 2 is coming: preventing breakage, updating your code**
============================================================================================

- https://pythonspeed.com/articles/numpy-2/

If you’re writing scientific or data science code with Python, there’s a
good chance you’re using NumPy, directly or indirectly. Pandas, Scikit-Image,
SciPy, Scikit-Learn, AstroPy… these and many other packages depend on NumPy.

NumPy 2 is a new major release, with a release candidate coming out February 1st
2024, and a final release a month or two later. Importantly, it’s backwards
incompatible; not in a major way, but enough that some work might be required
to upgrade. And that means you need to make sure your application doesn’t
break when NumPy 2 comes out.
