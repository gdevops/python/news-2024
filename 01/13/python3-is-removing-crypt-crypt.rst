

.. _python_3_13_crypt:

============================================================================================
2024-01-13 Python3 is removing crypt.crypt and not replacing it with anything ¯\_(ツ)_/¯
============================================================================================

- https://eighty-twenty.org/files/shacrypt512.py
- https://mastodon.social/@python_discussions/111750253128685532

Python 3.13 will, for inscrutable reasons, remove the crypt module from
the standard library. The excuses given in PEP 0594 boil down to
“here are some good reasons why new code shouldn’t use this module.”

What about existing code? Ah well.

So anyway, for those of us who need some way of generating $6$ SHAcrypt
SHA-512 shadow-password-database entries from Python, stick the following
module into your codebase (you can also download it, shacrypt512.py)
and replace code like

::

    crypt.crypt(password, salt=crypt.METHOD_SHA512)
    crypt.crypt(password, '$6$salt$...')
    crypt.crypt(password, '$6$salt$...') == '$6$salt$...'

with

    shacrypt512.shacrypt(password.encode('utf-8'))
    shacrypt512.shacrypt(password.encode('utf-8'), b'salt')
    shacrypt512.password_ok(password.encode('utf-8'), '$6$salt$...')

respectively.
