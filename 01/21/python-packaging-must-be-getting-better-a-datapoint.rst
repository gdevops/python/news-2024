

.. _plat_2024_01_21:

==================================================================================
2024-01-21 Python packaging must be getting better - a datapoint by Luke Plant
==================================================================================

- https://lukeplant.me.uk/blog/posts/python-packaging-must-be-getting-better-a-datapoint/


I’m developing some Python software for a client, which in its current early
state is desktop software that will need to run on Windows.

So far, however, I have done all development on my normal comfortable Linux
machine. I haven’t really used Windows in earnest for more than 15 years
– to the point where my wife happily installs Linux on her own machine,
knowing that I’ll be hopeless at helping her fix issues if the OS is Windows
– and certainly not for development work in that time. So I was expecting
a fair amount of pain.

There was certainly a lot of friction getting a development environment set
up. RealPython.com have a great guide which got me a long way, but even that
had some holes and a lot of inconvenience, mostly due to the fact that, on the
machine I needed to use, my main login and my admin login are separate. (I’m
very lucky to be granted an admin login at all, so I’m not complaining). And
there are lots of ways that Windows just seems to be broken, but that’s
another blog post.

When it came to getting my app running, however, I was very pleasantly surprised.

At this stage in development, I just have a rough requirements.txt that
I add Python deps to manually. This might be a good thing, as I avoid the
pain/confusion of some of the additional layers people have added, which I’m
mostly avoiding until things settle down a bit.

So after installing Python and creating a virtual environment on Windows,
I ran pip install -r requirements.txt, expecting a world of pain, especially
as I already had complex non-Python dependencies, including Qt5 and VTK. I
had specified both of these as simple Python deps via the wrappers pyqt5 and
vtk in my requirements.txt, and nothing else, with the attitude of “well I
may as well dream this is going to work”.

And in fact, it did! Everything just downloaded as binary wheels – rather
large ones, but that’s fine. I didn’t need compilers or QMake or header
files or anything.

And when I ran my app, apart from a dependency that I’d forgotten to add to
requirements.txt, everything worked perfectly first time. This was even more
surprising as I had put zero conscious effort into Windows compatibility. In
retrospect I realise that use of pathlib, which is automatic for me these
days, had helped me because it smooths over some Windows/Unix differences with
path handling.

Of course, this is a single datapoint. From other people’s reports there
are many, many ways that this experience may not be typical. But that it is
possible at all suggests that a lot of progress has been made and we are very
much going in the right direction. A lot of people have put a lot of work in
to achieve that, for which I’m very grateful!
